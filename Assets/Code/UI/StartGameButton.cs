﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class StartGameButton : MonoBehaviour
{
    public int playerCount;
    public TextMeshProUGUI text;
    public Slider playerCountSlider;
    public Slider generationSlider;
    public GameObject menu;

    public void Start()
    {
        SetPlayerCount();
    }

    public void SetPlayerCount()
    {
        playerCount = (int)playerCountSlider.value;
        text.text = "Start " + playerCount + " Player";
    }

    public void StartGameButtonPressed()
    {
        GameManager.Instance.GenerateMap();
    }

    public void OnGenerationSlider()
    {
        if(generationSlider.value > .99f)
        {
            GameManager.Instance.StartGame(playerCount);
            menu.SetActive(false);
        }
    }
}
