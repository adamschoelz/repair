﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class UnitRuntime : MonoBehaviour
{
    public UnitData data;
    public PlayerData owner;
    public CinemachineVirtualCamera focusCam;
    public Vector2 location;
    public UnitData.Size currentSize;
    public GameObject defaultModel;
    public GameObject altModel;
    public GameObject deadModel;
    public AudioClip attackSound;
    public AudioClip hurtSound;

    public int currentHealth;
    public int currentEnergy;
    public int actionsRemaining;
    public bool isHidden;
    public bool transfers = false;
    public bool IsDead { get; set; }
    public bool CanAct { get { return actionsRemaining > 0; } }

    public GameObject currentModel;

    public void Initialize(UnitData data, PlayerData creator, Vector2 creationLocation)
    {
        focusCam.transform.SetParent(null);
        this.data = data;
        this.owner = creator;
        this.transfers = data.transfers;
        currentEnergy = data.maxEnergy;
        currentHealth = data.maxHealth;
        currentSize = data.size;
        location = creationLocation;
        focusCam.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset.Set(PlayerCamera.MaxDistance, PlayerCamera.MaxHeight, -1 * PlayerCamera.MaxDistance);
        focusCam.gameObject.SetActive(false);
        focusCam.Follow = transform;
        focusCam.LookAt = transform;

        GameManager.Instance.Map.GetTile(location).Occupy(this);
        transform.position = GameManager.Instance.Map.GetWorldLocation(location);
        GameManager.Instance.Register.AddUnit(owner, this);
        GameManager.Instance.Map.GetTile(location).Occupy(this);
    }


    public void ModifyEnergy(int val)
    {
        currentEnergy += Mathf.Min(data.maxEnergy, val); ;
        if (currentEnergy < 0) Die();
    }

    public void ModifyHealth(int val)
    {
        currentHealth += Mathf.Min(data.maxHealth, val);
        if (currentHealth < 0) Die();
    }

    public void Die()
    {
        //Destroy(this.gameObject);
        GameManager.Instance.KillUnit(this);
    }

    public void SetVisible(bool playerVisible)
    {
        if(GameManager.Instance.CurrentPlayer == owner)
        {
            currentModel.gameObject.SetActive(true);
        }
        else if(playerVisible && !isHidden)
        {
            currentModel.gameObject.SetActive(true);
        }
        else
        {
            currentModel.gameObject.SetActive(false);
        }
    }

    public void Refresh()
    {
        actionsRemaining = data.maxActions;

        currentEnergy += data.energyRate;

        if(transfers)
        {
            int[] playerCounts = new int[GameManager.Instance.PlayerCount];
            for(int i = 0; i < 8; i++)
            {
                int x = ((i / 8) - 3) + (int)location.x;
                int y = ((i % 8) - 3) + (int)location.y;
                TileRuntime neighbor = GameManager.Instance.Map.GetTile(new Vector2(x, y));
                if(neighbor && neighbor.plantOccupant)
                {
                    playerCounts[GameManager.Instance.Register.GetPlayerIndex(neighbor.plantOccupant)]++;
                }
            }

            int maxIndex = GameManager.Instance.Register.GetPlayerIndex(this);
            for(int i = 0; i < playerCounts.Length; i++)
            {
                if(playerCounts[i] > playerCounts[maxIndex])
                {
                    maxIndex = i;
                }
            }

            if(maxIndex != GameManager.Instance.Register.GetPlayerIndex(this))
            {
                PlayerData newOwner = GameManager.Instance.Players[maxIndex];
                GameManager.Instance.SetOwner(this, newOwner);
            }
        }
    }

    public int CollectExcessEnergy()
    {
        int localProd = Mathf.Max(0, currentEnergy - data.maxEnergy);
        currentEnergy -= localProd;

        return localProd;
    }
}
