﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackVisual : MonoBehaviour
{
    public Transform visual;

    public void Initialize(Vector3 worldLocation, Vector2 track)
    {
        visual.position = worldLocation;
        Vector3 rot = visual.rotation.eulerAngles;
        float xDir = track.x != 0 ? Mathf.Sign(track.x) : 0;
        float yDir = track.y >= 0 ? 0 : 1;

        rot.y = (xDir * (45f)) + (180 * yDir);
        visual.localScale *= track.magnitude;
    }
}
