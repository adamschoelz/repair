﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewAmbushData", menuName = "Repair/Ability/Ambush")]
public class AmbushAbility : AttackAbility
{
    private bool activatedAmbush;

    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        for(int i = 1; i < range; i++)
        {
            for(int j = 0; j < i; j++)
            {
                //iterate through every tile and add this as ambusher
                TileRuntime tile = GameManager.Instance.Map.GetTile(new Vector2(activator.location.x + i, activator.location.y + j));
                tile.SetAmbush(activator);
            }
        }
    }

    public override void OnActivate(UnitRuntime activator)
    {
        if (activatedAmbush)
        {
            base.OnActivate(activator);
        }
    }

    public void SpringAmbush(UnitRuntime activator, UnitRuntime target, TileRuntime tile)
    {
        if (GameManager.Instance.CurrentPlayer == activator.owner) return;
        GameManager.Instance.Stop();
        targetedUnit = target;
        targetSet = true;
        this.target = tile.location;
        worldTarget = tile.transform.position;
        GameManager.Instance.DisplayOutline(this.target, GridOutline.OutlineType.Red);
        timeInCurrentLocation = 0;

        GameManager.Instance.ActivateAbility(this);
    }
}
