﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTrackingData", menuName = "Repair/Ability/Hide")]
public class HideAbility : AbilityData
{

    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        activator.isHidden = !activator.isHidden;
    }

    public override bool IsFinished(UnitRuntime activator)
    {
        return true;
    }
}
