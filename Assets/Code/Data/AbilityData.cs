﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class AbilityData : ScriptableObject
{
    new public string name;
    public string description;
    public Sprite icon;
    public int unitEnergyCost;
    public int playerEnergyCost;
    public GridOutline.OutlineType selectionColor = GridOutline.OutlineType.Green;
    public AudioClip sound;

    public virtual void EnterActivate(UnitRuntime activator) 
    {
    }
    public virtual void OnActivate(UnitRuntime activator) { }
    public virtual void ExitActivate(UnitRuntime activator) 
    {
        activator.actionsRemaining--;
        activator.ModifyEnergy(unitEnergyCost * -1);
        activator.owner.CurrentEnergy -= playerEnergyCost;
    }
    public virtual bool CanBeUsed(UnitRuntime activator)
    {
        bool energyReq = activator.owner.CurrentEnergy >= playerEnergyCost && activator.currentEnergy >= unitEnergyCost;
        bool actionReq = activator.CanAct && !GameManager.Instance.AbilityActive;

        return energyReq && actionReq;
    }

    public abstract bool IsFinished(UnitRuntime activator);
}
