﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class AbilityDisplay : MonoBehaviour
{
    public RectTransform displayPanel;
    public TextMeshProUGUI abilityName;
    public TextMeshProUGUI abilityDescription;
    public TextMeshProUGUI energyCost;
    public Image abilityIcon;
    public AbilityData displayingAbility;

    public void DisplayAbility(AbilityData a)
    {
        abilityName.text = a.name;
        abilityDescription.text = a.description;
        energyCost.text = a.unitEnergyCost.ToString() + " / " + a.playerEnergyCost.ToString();
        if(!a.CanBeUsed(GameManager.Instance.SelectedUnit))
        {
            energyCost.color = Color.red;
        }
        else
        {
            displayingAbility = a;
            energyCost.color = Color.white;
        }
        if (a.icon != null) abilityIcon.sprite = a.icon;
        displayPanel.gameObject.SetActive(true);
    }

    public void Activate()
    {
        if (displayingAbility != null)
        {
            GameManager.Instance.ActivateAbility(displayingAbility);
            Hide();
        }
    }

    public void Hide()
    {
        displayPanel.gameObject.SetActive(false);
        displayingAbility = null;
    }
}
