﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GenerationSlider : MonoBehaviour
{
    public Slider slider;

    public void Update()
    {
        float total = GameManager.Instance.Map.xDimension * GameManager.Instance.Map.yDimension;
        slider.value = GameManager.Instance.Map.tilesGenerated / total;
    }
}
