﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerData : MonoBehaviour
{
    public int CurrentEnergy { get; set; }
    public UnitData[] startingUnits;
    public Vector2[] startingUnitLocations;
    public bool[] visibilityMask;
    public Vector2 startingLocation;
    public int startingEnergy = 15;

    public void UpdateVisibilityMask()
    {
        int x = GameManager.Instance.Map.xDimension;
        int y = GameManager.Instance.Map.yDimension;

        if (visibilityMask == null || visibilityMask.Length != GameManager.Instance.Map.tilesGenerated)
        {
            visibilityMask = new bool[GameManager.Instance.Map.tilesGenerated];
        }

        List<UnitRuntime> units = GameManager.Instance.Register.GetPlayerUnits(this);

        //TODO: transition visibility mask to int, and add or subtract one based on old/new position
        for (int i = 0; i < visibilityMask.Length; i++)
        {
            int length = visibilityMask.Length;
            Vector2 coordinate = new Vector2(i / length, i % length);
            visibilityMask[i] = false;
            foreach(UnitRuntime u in units)
            {
                int dist = Mathf.FloorToInt(Vector2.Distance(u.location, coordinate));
                if (dist < (u.data.sightRadius+1))
                {
                    visibilityMask[i] = true;
                    break;
                }
            }

        }
    }
}
