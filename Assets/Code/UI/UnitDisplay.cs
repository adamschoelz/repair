﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UnitDisplay : MonoBehaviour
{
    public RectTransform unitDisplayPanel;
    public TextMeshProUGUI unitName;
    public TextMeshProUGUI unitDesc;
    public TextMeshProUGUI unitHealthStatus;
    public TextMeshProUGUI unitEnergyStatus;
    public TextMeshProUGUI actionsRemaining;

    public void DisplayUnit(UnitRuntime unit)
    {
        unitName.text = unit.data.name;
        unitDesc.text = unit.data.description;
        unitHealthStatus = BinValue(unitHealthStatus, unit.currentHealth, unit.data.maxHealth);
        unitEnergyStatus = BinValue(unitEnergyStatus, unit.currentEnergy, unit.data.maxActions);
        actionsRemaining.text = unit.actionsRemaining.ToString() + " / " + unit.data.maxActions;
    }

    public void HideDisplay()
    {
        unitDisplayPanel.gameObject.SetActive(false);
    }

    private TextMeshProUGUI BinValue(TextMeshProUGUI toBin, int value, int maxVal)
    {
        if ((float)value / (float)maxVal < 0.15f)
        {
            toBin.text = "Very Low";
            toBin.color = Color.red;
        }
        else if ((float)value / (float)maxVal < 0.35f)
        {
            toBin.text = "Low";
            toBin.color = Color.magenta;
        }
        else if ((float)value / (float)maxVal < 0.65f)
        {
            toBin.text = "Fair";
            toBin.color = Color.yellow;
        }
        else
        {
            toBin.text = "Good";
            toBin.color = Color.green;
        }
        return toBin;
    }
}
