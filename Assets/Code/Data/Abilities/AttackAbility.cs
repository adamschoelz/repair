﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[CreateAssetMenu(fileName = "NewAttackData", menuName = "Repair/Ability/Attack")]
public class AttackAbility : MoveAbility
{
    public int damage;
    public float[] sizeSuccessMatrix;
    public float attackTime;

    protected bool targetReached;
    protected float currentAttackTime;
    protected UnitRuntime targetedUnit;
    protected bool attackComplete;

    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        targetReached = false;
        currentAttackTime = 0;
        attackComplete = false;
    }

    public override void OnActivate(UnitRuntime activator)
    {
        targetReached = base.IsFinished(activator);
        
        if (!targetSet && !targetReached)
        {
            TileRuntime tile = GameManager.Instance.UI.GetTileUnderMouse();
            if (tile == null) return;
            int dist = Mathf.FloorToInt(Vector2.Distance(activator.location, tile.location));
            bool validTarget = tile.occupants.Count > 1 || (tile.occupants.Count > 0 && !tile.occupants.Contains(activator));
            if (tile.occupants.Count > 0 && dist <= range && validTarget)
            {
                targetedUnit = tile.occupants[0];
                foreach(UnitRuntime occupant in tile.occupants)
                {
                    //prefer to attack enemy units && do not attack self
                    if (occupant.owner != activator.owner || (occupant != activator && targetedUnit == activator))
                    {
                        targetedUnit = occupant;
                    }
                }
                targetSet = true;
                target = tile.location;
                worldTarget = tile.transform.position;
                GameManager.Instance.DisplayOutline(target, GridOutline.OutlineType.Red);
                timeInCurrentLocation = 0;
            }
        }
        if(targetSet && !targetReached)
        {
            base.OnActivate(activator);
        }
        if (targetReached)
        {
            currentAttackTime += Time.deltaTime;
            if(currentAttackTime > attackTime)
            {
                //attempt damage
                if (UnityEngine.Random.value < sizeSuccessMatrix[(int)targetedUnit.currentSize])
                {
                    targetedUnit.ModifyHealth(damage*-1);
                    Debug.Log("Attack result: " + targetedUnit.currentHealth);
                }
                attackComplete = true;
            }
        }
    }

    public override void ExitActivate(UnitRuntime activator)
    {
        base.ExitActivate(activator);
        targetReached = false;
        currentAttackTime = 0;        
    }

    public override bool IsFinished(UnitRuntime activator)
    {
        return attackComplete;
    }

    public void OnValidate()
    {
        int sizes = Enum.GetNames(typeof(UnitData.Size)).Length;
        if (sizeSuccessMatrix.Length != sizes)
        {
            Array.Resize<float>(ref sizeSuccessMatrix, sizes);
        }
    }
}
