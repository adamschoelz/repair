﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitRegister : MonoBehaviour
{
    private List<UnitRuntime>[] unitLists;

    private int selectedIndex;

    public List<UnitRuntime> CurrentUnits { get { return unitLists[GameManager.Instance.CurrentPlayerIndex]; } }
    public UnitRuntime SelectedUnit 
    {
        get 
        {
            /*if (selectedIndex > unitLists[GameManager.Instance.CurrentPlayerIndex].Count || selectedIndex < 0)
            {
                selectedIndex = 0;
            }*/
            selectedIndex = selectedIndex % unitLists[GameManager.Instance.CurrentPlayerIndex].Count;
            if (unitLists[GameManager.Instance.CurrentPlayerIndex].Count == 0) return null;
            return unitLists[GameManager.Instance.CurrentPlayerIndex][selectedIndex]; 
        } 
    }

    public void Initialize(PlayerData[] players)
    {
        unitLists = new List<UnitRuntime>[players.Length];
        selectedIndex = 0;
        for(int i = 0; i < players.Length; i++)
        {
            unitLists[i] = new List<UnitRuntime>();
        }
    }

    public void AddUnit(PlayerData player, UnitRuntime unit)
    {
        if (player == GameManager.Instance.Gaia) return;
        int playerIndex = GetPlayerIndex(player);
        unitLists[playerIndex].Add(unit);
    }

    public void AddUnits(PlayerData player, UnitRuntime[] units)
    {
        int playerIndex = GetPlayerIndex(player);
        for (int i = 0; i < units.Length; i++)
        {
            AddUnit(i, units[playerIndex]);
        }
    }

    public void AddUnit(int player, UnitRuntime unit)
    {
        unitLists[player].Add(unit);
    }

    public void AddUnits(int player, UnitRuntime[] units)
    {
        for(int i = 0; i < units.Length; i++)
        {
            AddUnit(i, units[player]);
        }
    }

    public bool RemoveUnit(UnitRuntime unit)
    {
        int index = GetPlayerIndex(unit);
        if (index > 0)
        {
            return unitLists[index].Remove(unit);
        }

        return false;
    }

    public UnitRuntime SelectUnit(UnitRuntime unit)
    {
        selectedIndex--;
        return SelectedUnit;
    }

    public UnitRuntime NextUnit(int player)
    {
        selectedIndex++;
        return SelectedUnit;
    }

    public UnitRuntime PreviousUnit(int player)
    {
        selectedIndex -= 1;
        selectedIndex = selectedIndex % unitLists[player].Count;
        
        return SelectedUnit;
    }

    public List<UnitRuntime> GetPlayerUnits(PlayerData player)
    {
        int index = GetPlayerIndex(player);

        if (index >= 0)
        {
            return unitLists[index];
        }

        return null;
    }

    public int GetPlayerIndex(UnitRuntime unit)
    {
        for(int i = 0; i < GameManager.Instance.PlayerCount; i++)
        {
            if(unit.owner == GameManager.Instance.Players[i])
            {
                return i;
            }
        }

        return -1;
    }

    public int GetPlayerIndex(PlayerData player)
    {
        for (int i = 0; i < GameManager.Instance.PlayerCount; i++)
        {
            if (player == GameManager.Instance.Players[i])
            {
                return i;
            }
        }
        return -1;
    }


}
