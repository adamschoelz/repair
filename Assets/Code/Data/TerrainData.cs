﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainData : ScriptableObject
{
    public int moveCost;
    public int growthCost;
}
