﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="NewMoveData", menuName ="Repair/Ability/MoveData")]
public class MoveAbility : AbilityData
{
    public int range;
    public float timePerSquare;
    protected Vector2 target;
    protected Vector3 worldTarget;
    protected bool targetSet;
    protected float timeInCurrentLocation;
    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        targetSet = false;
        Debug.Log("Entered move");
        GameManager.Instance.DisplayOutline(activator.location, range, selectionColor);
    }

    public override void OnActivate(UnitRuntime activator)
    {
        base.OnActivate(activator);
        if(!targetSet)
        {
            TileRuntime tile = GameManager.Instance.UI.GetTileUnderMouse();
            if (tile == null)
            {
                return;
            }

            int dist = Mathf.FloorToInt(Vector2.Distance(activator.location, tile.location));
            if (tile.CanOccupy(activator) && dist <= range)
            {
                targetSet = true;
                target = tile.location;
                worldTarget = tile.transform.position;
                GameManager.Instance.DisplayOutline(target, GridOutline.OutlineType.Green);
                timeInCurrentLocation = 0;
                GameManager.Instance.Map.GetTile(activator.location).Vacate(activator);
            }
        }
        if(targetSet && !IsFinished(activator))
        {
            Move(activator);
        }
    }

    public override void ExitActivate(UnitRuntime activator)
    {
        base.ExitActivate(activator);
        //GameManager.Instance.Map.
        targetSet = false;
    }

    public override bool CanBeUsed(UnitRuntime activator)
    {
        return base.CanBeUsed(activator) && !activator.isHidden;
    }

    public override bool IsFinished(UnitRuntime activator)
    {
        return targetSet && activator.location == target;
    }

    protected void Move(UnitRuntime activator)
    {
        timeInCurrentLocation += Time.deltaTime;
        Vector2 dir = target - activator.location;
        if(dir.x != 0) dir.x = dir.x / Mathf.Abs(dir.x);
        if(dir.y != 0) dir.y = dir.y / Mathf.Abs(dir.y);
        Debug.Log(dir);
        Vector3 currentWorldLocation = GameManager.Instance.Map.GetWorldLocation(activator.location);
        Vector3 targetWorldLocation = GameManager.Instance.Map.GetWorldLocation(activator.location + dir);
        if (timeInCurrentLocation > timePerSquare)
        {
            timeInCurrentLocation = 0;
            GameManager.Instance.Map.PassThrough(activator, activator.location + dir, dir);
            activator.owner.UpdateVisibilityMask();
            activator.transform.LookAt(targetWorldLocation);
        }
        else
        {
            //lerp position
            float fraction = timeInCurrentLocation / timePerSquare;
            activator.transform.position = Vector3.Lerp(currentWorldLocation, targetWorldLocation, fraction);
        }

    }
}
