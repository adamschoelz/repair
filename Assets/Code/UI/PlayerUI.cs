﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour
{
    public Text playerNameText;
    public Text energyText;
    public Button[] abilityButtons;
    public PlayerCamera playerCamera;
    public UnitDisplay unitDisplay;
    public AbilityDisplay abilityDisplay;
    public NotificationPanel notificationPanel;

    public void DisplayPlayer(PlayerData player)
    {
        playerNameText.text = player.name;
        energyText.text = player.CurrentEnergy.ToString();
        ViewUnit(GameManager.Instance.Register.SelectedUnit);
    }

    public void GenerateAbilityMenu(UnitRuntime unit)
    {
        for(int i = 0; i < unit.data.abilities.Length; i++)
        {
            AbilityData a = unit.data.abilities[i];
            abilityButtons[i].gameObject.SetActive(true);
            if (a.icon != null) abilityButtons[i].GetComponentsInChildren<Image>()[1].sprite = a.icon;

            if (a.CanBeUsed(unit))
            {
                abilityButtons[i].interactable = true;
            }
            else
            {
                abilityButtons[i].interactable = false;
            }
        }
        for(int i = unit.data.abilities.Length; i < abilityButtons.Length; i++)
        {
            abilityButtons[i].gameObject.SetActive(false);
        }
    }

    public void ActivateAbility(int index)
    {
        AbilityData ability = GameManager.Instance.SelectedUnit.data.abilities[index];
        abilityDisplay.DisplayAbility(ability);
    }

    public void ViewUnit(UnitRuntime unit)
    {
        playerCamera.Focus(unit);
        unitDisplay.DisplayUnit(unit);
        GenerateAbilityMenu(unit);
        if (!GameManager.Instance.AbilityActive)
        {
            if(unit.CanAct)
            {
                GameManager.Instance.DisplayOutline(unit.location, GridOutline.OutlineType.Green);
            }
            else
            {
                GameManager.Instance.DisplayOutline(unit.location, GridOutline.OutlineType.Red);
            }
        }
    }

    public void NextUnit()
    {
        ViewUnit(GameManager.Instance.NextUnit);
    }

    public void PreviousUnit()
    {
        ViewUnit(GameManager.Instance.PreviousUnit);
    }

    public void EndTurn()
    {
        GameManager.Instance.EndTurn();
    }

    public TileRuntime GetTileUnderMouse()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = playerCamera.controlCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            Debug.DrawRay(ray.origin, ray.direction * 1000f, Color.green, 5);
            if(Physics.Raycast(ray, out hit))
            {
                TileRuntime tile = hit.collider.GetComponent<TileRuntime>();
                if (tile != null)
                {

                    return tile;
                }
            }
        }

        return null;
    }

    public UnitRuntime GetPlantUnderMouse()
    {
        TileRuntime tile = GetTileUnderMouse();
        if (tile != null) return tile.plantOccupant;

        return null;
    }

    public UnitRuntime[] GetAnimalUnderMouse()
    {
        TileRuntime tile = GetTileUnderMouse();
        if (tile != null) return tile.occupants.ToArray();

        return null;
    }

    public void Notify(string message)
    {
        notificationPanel.ShowNotification(message);
    }
}
