﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridOutline : MonoBehaviour
{
    public LineRenderer gridOutline;
    public LineRenderer squareOutline;
    public enum OutlineType { Green, Yellow, Orange, Red };
    public Material[] materials;

    public void DrawGrid(TerrainGrid grid, Vector2 spot, int range, OutlineType color = OutlineType.Green)
    {
        List<Vector3> points = new List<Vector3>();

        int baseX = (int)spot.x;
        int baseY = (int)spot.y;
        Vector2 loc = new Vector2(baseX, baseY + range);
        List<Vector2> tiles = new List<Vector2>();
        //each side is a 45-45-90 right triangle
        int pointCount = Mathf.FloorToInt((range+1) * Mathf.Sqrt(2)) + 1;

        //add initial point
        //points = AddExtents(grid, points, loc, 0);
        //points = AddExtents(grid, points, loc, 1);
        //points = AddExtents(grid, points, loc, 0);
        //points = AddExtents(grid, points, loc, 2);

        /* order of extents:
         0 2
         1 3
         */

        //TODO: finish this later
        /*Vector3 worldLocation = GameManager.Instance.Map.GetWorldLocation(spot);
        Collider[] hit = Physics.OverlapBox(worldLocation, Vector3.one * range, Quaternion.Euler(0, 45, 0));
        Vector3[] points = new Vector3[1000];
        foreach (Collider c in hit)
        {
            TileRuntime t = c.GetComponent<TileRuntime>();
            if (t && Vector2.Distance(spot, t.location) - range < 0.2f)
            {
                tiles.Add(t.location);
                PlaceSide(grid, ref points, t, spot);
            }
        }

        
        for(int i = 0; i < points.Length; i++)
        {
            Vector2 dir = spot - t.location;
            if (dir.x != 0) dir.x = Mathf.Sign(dir.x);
            if (dir.y != 0) dir.y = Mathf.Sign(dir.y);

        }
        //widen
        for (int i  = 0; i < range*2+1; i++)
        {
            int width = (i*2);
            int height = range - i;
            offset.x = Mathf.Clamp(width, 0, grid.xDimension);
            offset.y = Mathf.Clamp(height, 0, grid.yDimension);
            negOffset.x = Mathf.Clamp(-1 * width, 0, grid.xDimension);
            negOffset.y = Mathf.Clamp(height, 0, grid.yDimension);
            Vector2 posSpot = spot + offset;
            Vector2 negSpot = spot + negOffset;
            
            //add horizontals

            //add verticals
        }*/
        //narrow
        for (int i = 0; i < pointCount; i++)
        {
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 0 : 1);
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 1 : 3);
            if(i % 2 != 0 && i != pointCount - 1)
            {
                loc.x = Mathf.Max(0, loc.x - 1);
                loc.y = Mathf.Max(0, loc.y - 1);
            }

            //Debug.Log(loc);

        }

        //add left to bot points
        for (int i = 0; i < pointCount; i++)
        {
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 3 : 1);
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 2 : 3);

            if (i % 2 == 0 && i != pointCount-1)
            {
                loc.x = Mathf.Min(grid.xDimension - 1, loc.x + 1);
                loc.y = Mathf.Max(0, loc.y - 1);
            }

        }

        //points = AddExtents(grid, points, loc, 0);
        //points = AddExtents(grid, points, loc, 2);

        //add bot to right
        for (int i = 0; i < pointCount; i++)
        {
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 0 : 3);
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 2 : 2);

            if (i % 2 == 0 && i != pointCount - 1)
            {
                loc.x = Mathf.Min(grid.xDimension - 1, loc.x + 1);
                loc.y = Mathf.Min(grid.yDimension - 1, loc.y + 1);
            }
        }

        //add right to top
        for (int i = 0; i < pointCount; i++)
        {
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 0 : 2);
            points = AddExtents(grid, points, loc, i % 2 == 0 ? 1 : 0);

            if (i % 2 == 0 && i != pointCount - 1)
            {
                loc.x = Mathf.Max(0, loc.x - 1);
                loc.y = Mathf.Min(grid.yDimension - 1, loc.y + 1);
            }
        }

        //Debug.Log(points.Count);
        gridOutline.material = materials[(int)color];
        gridOutline.positionCount = points.Count;
        gridOutline.SetPositions(points.ToArray());
    }

    public void DrawSquare(TerrainGrid grid, Vector2 location, OutlineType color = OutlineType.Green)
    {
        Vector3[] extents = grid.GetTileExtents(location);
        gridOutline.material = materials[(int)color];
        gridOutline.positionCount = extents.Length + 1;
        gridOutline.SetPosition(0, extents[0]);
        gridOutline.SetPosition(1, extents[1]);
        gridOutline.SetPosition(2, extents[3]);
        gridOutline.SetPosition(3, extents[2]);
        gridOutline.SetPosition(4, extents[0]);

    }
    /* order of extents:
     0 2
     1 3
     */
    private void PlaceSide(TerrainGrid grid, ref Vector3[] points, TileRuntime t, Vector2 origin)
    {
        Vector2 dir = origin - t.location;
        if (dir.x != 0) dir.x = Mathf.Sign(dir.x);
        if (dir.y != 0) dir.y = Mathf.Sign(dir.y);

        switch(t.Edge)
        {
            case TileRuntime.EdgeType.None:
                break;
            case TileRuntime.EdgeType.Top:
                break;
            case TileRuntime.EdgeType.Bottom:
                break;
            case TileRuntime.EdgeType.Left:
                break;
            case TileRuntime.EdgeType.Right:
                break;
            case TileRuntime.EdgeType.Corner:
                break;
            default: break;
        }
    }

    private void PlaceSideByDirection(TerrainGrid grid, ref Vector3[] points, Vector2 dir, Vector2 location)
    {
        int index = (int)(location.x * GameManager.Instance.Map.xDimension + location.y);
        if (dir.x > 0)
        {
            
        }
        else if(dir.x == 0)
        {

        }
        else
        {

        }
    }

    private List<Vector3> AddExtents(TerrainGrid grid, List<Vector3> points, Vector2 location, int side)
    {
        Vector3[] extents = grid.GetTileExtents(location);
        //side: start at that entry and move counterclockwise
        if (extents != null)
        {
            points.Add(extents[side]);
        }
        else
        {
            Debug.Log("Bad request from " + location);
        }
        //Debug.Log("Adding: " + points.Count);
        return points;
    }
}
