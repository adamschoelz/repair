﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameManager : MonoBehaviour
{
    public TerrainGrid Map;
    public UnitRegister Register;
    public PlayerUI UI;
    public GridOutline Outline;
    public PlayerData[] players;
    public int gameLength;
    public bool startOnAwake;

    private static GameManager instance;
    private int currentPlayer;
    private int currentRound;
    private AbilityData currentAbility;
    private List<GameObject> toDestroy;
    private bool waitingOnAcknowledge;

    public static GameManager Instance
    {
        get { return instance; }
    }

    public PlayerData CurrentPlayer { get {return players[currentPlayer];} }
    public int CurrentPlayerIndex { get { return currentPlayer; } }
    public int PlayerCount { get { return players.Length; } }
    public PlayerData[] Players { get { return players; } }
    public PlayerData Gaia;
    public AudioSource soundSource;

    public UnitRuntime NextUnit 
    { 
        get 
        {
            if(!GameManager.Instance.AbilityActive) Register.NextUnit(currentPlayer);
            return SelectedUnit;
        } 
    }
    public UnitRuntime PreviousUnit
    {
        get
        {
            if (!AbilityActive) Register.PreviousUnit(currentPlayer);
            return SelectedUnit;
        }
    }

    public UnitRuntime SelectedUnit { get { return Register.SelectedUnit; } }
    public bool AbilityActive { get { return currentAbility != null; } }

    public void Awake()
    {
        instance = this;
        currentAbility = null;
        toDestroy = new List<GameObject>();
    }

    public void Start()
    {
        if (startOnAwake) StartGame();
    }

    public void Update()
    {
        if(currentAbility != null && !currentAbility.IsFinished(SelectedUnit))
        {
            currentAbility.OnActivate(SelectedUnit);
        }
        else if(currentAbility != null)
        {
            Stop();
        }
        if(Input.GetMouseButtonDown(1))
        {
            Cancel();
        }
    }

    public void ActivateAbility(AbilityData ability)
    {
        ability.EnterActivate(SelectedUnit);
        if (!ability.IsFinished(SelectedUnit))
        {
            currentAbility = ability;
            //UI.ViewUnit(SelectedUnit);
        }

    }

    public void ActivateAbility(AbilityData ability, UnitRuntime interrupter)
    {
        Register.SelectUnit(interrupter);
        ActivateAbility(ability);
    }

    public void Cancel()
    {
        currentAbility = null;
        
        if (SelectedUnit.owner != CurrentPlayer) Register.NextUnit(currentPlayer);

        UI.DisplayPlayer(CurrentPlayer);
    }

    public void Stop()
    {
        if (currentAbility != null) currentAbility.ExitActivate(SelectedUnit);
        currentAbility = null;

        if (SelectedUnit.owner != CurrentPlayer) Register.NextUnit(currentPlayer);

        UI.DisplayPlayer(CurrentPlayer);
    }

    public void GenerateMap()
    {
        Map.GenerateMap();
    }

    public void StartGame(int playerCount)
    {
        List<PlayerData> totalPlayers = new List<PlayerData>(GetComponentsInChildren<PlayerData>());
        players = totalPlayers.GetRange(0, playerCount).ToArray();
        Register.Initialize(players);
        for (int i = 0; i < players.Length; i++) CreateStartingUnits(i);
        UI.Notify(CurrentPlayer.name + "'s turn");
        waitingOnAcknowledge = true;
        UI.gameObject.SetActive(true);
    }

    public void StartGame()
    {
        StartGame(2);
    }

    public void CreateStartingUnits(int player)
    {
        players[player].CurrentEnergy = players[player].startingEnergy;
        Vector2 baseLocation = players[player].startingLocation;
        for(int i = 0; i < players[player].startingUnits.Length; i++)
        {
            //if location isn't occupied, spawn unit
            PlayerData starter = players[player];
            UnitRuntime u = Instantiate(starter.startingUnits[i].worldPrefab);
            u.Initialize(starter.startingUnits[i], starter, starter.startingUnitLocations[i] + baseLocation);
            Register.AddUnit(player, u);
        }
    }

    public void SpawnUnitForCurrentPlayer(UnitData unit, Vector2 location)
    {
        UnitRuntime u = Instantiate(unit.worldPrefab);
        u.Initialize(unit, CurrentPlayer, location);
    }

    public void SpawnGaiaUnit(UnitData unit, Vector2 location)
    {
        UnitRuntime u = Instantiate(unit.worldPrefab);
        u.Initialize(unit, Gaia, location);
    }

    public void EndGame()
    {
        Debug.Log("Game Ended!");
    }

    public void StartTurn()
    {
        //add energy prod onto new player
        /*if(Register.CurrentUnits.Count == 0 || SelectedUnit == null)
        {
            UI.Notify("You have been eliminated!");
            EndTurn();
            return;
        }*/
        foreach(UnitRuntime u in Register.CurrentUnits)
        {
            u.Refresh();
        }
        CurrentPlayer.CurrentEnergy += Map.GenerateEnergy(CurrentPlayer);
        //display player ui etc
        UI.DisplayPlayer(CurrentPlayer);
        UI.ViewUnit(SelectedUnit);
    }

    public void KillUnit(UnitRuntime destroyed)
    {
        SetOwner(destroyed, Gaia);
        destroyed.ModifyHealth(destroyed.data.maxHealth);
        destroyed.IsDead = true;
        if (destroyed.data.isPlant)
        {
            destroyed.gameObject.SetActive(false);
            Map.GetTile(destroyed.location).Vacate(destroyed);
        }
        else
        {
            destroyed.currentModel.SetActive(false);
            destroyed.currentModel = destroyed.deadModel;
            destroyed.currentModel.SetActive(true);
        }
    }

    public void EndTurn()
    {
        currentPlayer++;
        if(currentPlayer < PlayerCount)
        {
            UI.Notify(CurrentPlayer.name + "'s turn");
            waitingOnAcknowledge = true;
        }
        else
        {
            EndRound();
        }
    }

    public void EndRound()
    {
        currentPlayer = 0;
        currentRound++;
        UI.Notify("Round " + currentRound + " has ended!");
        waitingOnAcknowledge = true;
        Map.RefreshMap();

        if (currentRound > gameLength && gameLength > 0)
        {
            EndGame();
        }
    }

    public void AcknowedgeNotification()
    {
        if (waitingOnAcknowledge) StartTurn();
    }

    public void DisplayOutline(Vector2 location, int range, GridOutline.OutlineType color)
    {
        Outline.DrawGrid(Map, location, range, color);
        Outline.gameObject.SetActive(true);
    }

    public void DisplayOutline(Vector2 location, int range)
    {
        Outline.DrawGrid(Map, location, range);
        Outline.gameObject.SetActive(true);
    }

    public void DisplayOutline(Vector2 location, GridOutline.OutlineType color)
    {
        Outline.DrawSquare(Map, location, color);
        Outline.gameObject.SetActive(true);
    }

    public void HideOutline()
    {
        Outline.gameObject.SetActive(false);
    }

    public void SetOwner(UnitRuntime unit, PlayerData owner)
    {
        Register.RemoveUnit(unit);
        unit.owner = owner;
        Register.AddUnit(owner, unit);
    }

    public void PlaySound(AudioClip clip)
    {
        soundSource.PlayOneShot(clip);
    }

}
