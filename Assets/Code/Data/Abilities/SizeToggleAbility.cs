﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewSizeToggleData", menuName = "Repair/Ability/Size Toggle")]
public class SizeToggleAbility : AbilityData
{
    public UnitData.Size changedSize;
    public GameObject changedSizeModel;
    public bool hasChangedSize;

    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        hasChangedSize = false;
    }

    public override void OnActivate(UnitRuntime activator)
    {
        base.OnActivate(activator);
        if(activator.currentSize == changedSize)
        {
            activator.currentSize = activator.data.size;
            activator.altModel.SetActive(false);
            activator.defaultModel.SetActive(true);
            hasChangedSize = true;
        }
        else if(activator.currentSize == activator.data.size)
        {
            activator.currentSize = changedSize;
            activator.altModel.SetActive(false);
            activator.defaultModel.SetActive(true);
            hasChangedSize = true;
        }
    }

    public override bool IsFinished(UnitRuntime activator)
    {
        return hasChangedSize;
    }
}
