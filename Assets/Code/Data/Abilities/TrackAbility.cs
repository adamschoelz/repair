﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewTrackingData", menuName = "Repair/Ability/Track")]
public class TrackAbility : AbilityData
{
    public int trackingRevealCount;
    public TrackVisual visual;

    private List<TrackVisual> createdVisuals;


    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        Vector2[] tracks = GameManager.Instance.Map.GetTile(activator.location).GetHistory(trackingRevealCount);
        createdVisuals = new List<TrackVisual>();
        foreach (Vector2 t in tracks)
        {
            TrackVisual tv = Instantiate(visual);
            tv.Initialize(GameManager.Instance.Map.GetWorldLocation(activator.location), t);
            createdVisuals.Add(tv);
        }
    }

    public override void ExitActivate(UnitRuntime activator)
    {
        base.ExitActivate(activator);
        foreach (TrackVisual t in createdVisuals)
        {
            Destroy(t);
        }
        createdVisuals.Clear();
    }

    public override bool IsFinished(UnitRuntime activator)
    {

        return Input.GetMouseButtonDown(0);
    }
}
