﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class PlayerCamera : MonoBehaviour
{
    public static float MaxHeight = 20f;
    public static float MinHeight = 3f;
    public static float MaxDistance = 15f;
    public Camera controlCamera;
    public CinemachineVirtualCamera focusCamera;
    public float scrollSpeed = 2f;
    public float revolveSpeed = 2f;

    private Vector3 oldPosition;

    public void Update()
    {
        oldPosition = Input.mousePosition;
        if (focusCamera)
        {
            ZoomCamera(); 
            //RevolveCamera();

        }
    }

    public void Focus(UnitRuntime onUnit)
    {
        Vector3 pos = Vector3.zero;
        if (focusCamera != null)
        {
            focusCamera.gameObject.SetActive(false);
            pos = focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;

        }

        focusCamera = onUnit.focusCam;
        if(pos != Vector3.zero)
        {
            focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = pos;
        }
        focusCamera.gameObject.SetActive(true);
    }


    //functions return camera deltas based on input
    public void RevolveCamera()
    {
        if(Input.GetMouseButton(0))
        {
            Vector3 delta = Input.mousePosition - oldPosition;
            Vector3 pos = Vector3.zero;
            Vector3 follow = focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;

            pos = transform.right * Mathf.Sign(delta.x) * revolveSpeed * Time.deltaTime;
            pos.y = 0;
            float scale = pos.magnitude / follow.magnitude;
            //pos *= scale;
            Debug.Log(focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset);
            focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = follow + pos;
            Debug.Log(focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset);

        }
        return;
    }

    public void ZoomCamera()
    {
        Vector2 scroll = Input.mouseScrollDelta;
        Vector3 pos = focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset;
        float newY = pos.y + (-1 * scroll.y * Time.deltaTime * scrollSpeed);
        newY = Mathf.Clamp(newY, MinHeight, MaxHeight);
        float frac = newY / MaxHeight;

        pos.x = frac * MaxDistance;
        pos.y = newY;
        pos.z = frac * MaxDistance;
        focusCamera.GetCinemachineComponent<CinemachineTransposer>().m_FollowOffset = pos;

        return;
    }
}
