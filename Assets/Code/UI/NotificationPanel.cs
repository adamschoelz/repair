﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NotificationPanel : MonoBehaviour
{
    public TextMeshProUGUI note;

    public void ShowNotification(string notification)
    {
        note.text = notification;
        this.gameObject.SetActive(true);
    }

    public void Hide()
    {
        GameManager.Instance.AcknowedgeNotification();
        this.gameObject.SetActive(false);
    }
}
