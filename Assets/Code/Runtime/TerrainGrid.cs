﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainGrid : MonoBehaviour
{
    public int xDimension;
    public int yDimension;
    public TileRuntime tilePrefab;
    public Transform[] debugExtents;
    private TileRuntime[] tiles;
    public UnitData[] GaiaUnits;
    public int tilesGenerated;

    public void GenerateMap()
    {
        tiles = new TileRuntime[xDimension * yDimension];

        StartCoroutine(CreateTiles(tiles));
    }

    private IEnumerator CreateTiles(TileRuntime[] array)
    {
        int i = 0;
        while(i < tiles.Length)
        {
            tilesGenerated++;
            tiles[i] = Instantiate(tilePrefab);
            tiles[i].Initialize(this, i / xDimension, i % yDimension);
            i++;
            yield return null;
        }
        i = 0;
        while(i < Mathf.Sqrt(array.Length)/10f)
        {
            Vector2 location = new Vector2();
            location.x = xDimension * Random.value;
            location.y = yDimension * Random.value;
            GameManager.Instance.SpawnGaiaUnit(GaiaUnits[Random.Range(0, GaiaUnits.Length)], location);
            i++;
            yield return null;
        }
    }

    public void RefreshMap()
    {
        for (int i = 0; i < tiles.Length; i++)
        {
            tiles[i].RefreshTile();
        }
    }

    public void GenerateVisibility(PlayerData player)
    {
        player.UpdateVisibilityMask();
        for(int i = 0; i < tiles.Length; i++)
        {
            if(player.visibilityMask[i])
            {
                //tile is visible
                tiles[i].SetVisible(true);
            }
            else tiles[i].SetVisible(false);

        }
    }

    public int GenerateEnergy(PlayerData player)
    {
        player.UpdateVisibilityMask();
        int totalEnergy = 0;
        for (int i = 0; i < tiles.Length; i++)
        {
            int energy = tiles[i].GetEnergyForPlayer(player);
            totalEnergy += Mathf.Max(0, energy);
        }

        return totalEnergy;
    }

    public Vector3 GetWorldLocation(Vector2 at)
    {
        int coordinate = xDimension * (int)at.x + (int)at.y;
        return tiles[coordinate].transform.position;
    }

    public TileRuntime GetTile(Vector2 at)
    {
        int coordinate = xDimension * (int)at.x + (int)at.y;
        return tiles[coordinate];
    }

    public Vector2 GetBoardCoordinates(TileRuntime tile)
    {
        for(int i = 0; i < tiles.Length; i++)
        {
            if(tile == tiles[i])
            {
                return new Vector2(i / xDimension, i % yDimension);
            }
        }

        return Vector2.one * -1000;
    }

    public bool IsOccupied(Vector2 location)
    {
        int coordinate = xDimension * (int)location.x + (int)location.y;
        if(tiles[coordinate].occupants.Count > 0)
        {
            return true;
        }

        return false;
    }

    public bool CanOccupy(UnitRuntime unit, Vector2 location)
    {
        return tiles[(int)location.x * xDimension + (int)location.y].CanOccupy(unit);
    }

    public void PassThrough(UnitRuntime unit, Vector2 location, Vector2 direction)
    {
        TileRuntime tile = tiles[(int)location.x * xDimension + (int)location.y];
        unit.location = location;
        tile.PassThrough(direction, unit);
    }

    public void SetVisible(bool[] visibilityMask)
    {
        for(int i = 0; i < visibilityMask.Length; i++)
        {
            tiles[i].SetVisible(visibilityMask[i]);
        }
    }

    //starts top left and moves counterclockwise
    public Vector3[] GetTileExtents(Vector2 location)
    {
        int coordinate = xDimension * (int)location.x + (int)location.y;

        if (coordinate < 0 || coordinate > tiles.Length)
        {
            return null;
        }
        Vector3[] extents = new Vector3[4];
        Vector3 baseLoc = GetWorldLocation(location);

        extents[0].x = baseLoc.x + TileRuntime.TileSize / 2f;
        extents[0].y = 1;
        extents[0].z = baseLoc.z + TileRuntime.TileSize / 2f;

        extents[1].x = baseLoc.x - TileRuntime.TileSize / 2f;
        extents[1].y = 1;
        extents[1].z = baseLoc.z + TileRuntime.TileSize / 2f;
        //debugExtents[1].position = extents[1];

        extents[2].x = baseLoc.x + TileRuntime.TileSize / 2f;
        extents[2].y = 1;
        extents[2].z = baseLoc.z - TileRuntime.TileSize / 2f;
        //debugExtents[2].position = extents[2];

        extents[3].x = baseLoc.x - TileRuntime.TileSize / 2f;
        extents[3].y = 1;
        extents[3].z = baseLoc.z - TileRuntime.TileSize / 2f;
        //debugExtents[3].position = extents[3];


        return extents;
    }
}
