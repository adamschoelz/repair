﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewEatData", menuName = "Repair/Ability/Eat")]
public class EatAbility : AbilityData
{
    private float currentEatTime;
    private bool hasPrey;
    private UnitRuntime activePrey;

    public float eatTime;
    public int eatDamage;
    public int recoveryAmount;
    public UnitData[] prey;

    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        hasPrey = false;
        currentEatTime = 0;
    }

    public override void OnActivate(UnitRuntime activator)
    {
        base.OnActivate(activator);
        TileRuntime tile = GameManager.Instance.Map.GetTile(activator.location);
        foreach(UnitData d in prey)
        {
            foreach(UnitRuntime u in tile.occupants)
            {
                if (u.data == d && u.IsDead) activePrey = u;
            }
            if (tile.plantOccupant && tile.plantOccupant.data == d)
            {
                activePrey = tile.plantOccupant;
            }
        }
        if(activePrey)
        {
            currentEatTime += Time.deltaTime;
        }
    }

    public override bool CanBeUsed(UnitRuntime activator)
    {
        return base.CanBeUsed(activator) && 
            !(activator.currentSize == UnitData.Size.Flying) && 
            !(activator.currentSize == UnitData.Size.Burrowed);
    }

    public override void ExitActivate(UnitRuntime activator)
    {
        base.ExitActivate(activator);
        activator.ModifyEnergy(recoveryAmount);
        activePrey.ModifyHealth(eatDamage);
        hasPrey = false;
    }

    public override bool IsFinished(UnitRuntime activator)
    {
        return hasPrey && currentEatTime > eatTime;
    }
}
