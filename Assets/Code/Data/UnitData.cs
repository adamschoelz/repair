﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewUnit", menuName ="Repair/Unit Data")]
public class UnitData : ScriptableObject
{
    public new string name;
    public string description;
    public enum Size { Giant, Large, Medium, Small, Tiny, Flying, Burrowed };
    public int sightRadius;
    //any energy generated about maxEnergy is returned to the player
    public int maxEnergy;
    public int maxActions;
    public bool transfers;
    public int energyRate = -1;
    public bool isPlant = false;
    public bool alwaysVisible = false;
    public int maxHealth;
    public Size size;
    public AbilityData[] abilities;
    public UnitRuntime worldPrefab;
    public Material worldMaterial;
}
