﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MenuRotate : MonoBehaviour
{
    public Transform toRotate;
    public float rotatingSpeed;

    public void Awake()
    {
        toRotate.DORotate(Vector3.up * 360f, 360f / rotatingSpeed).SetLoops(-1);
    }
}
