﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileRuntime : MonoBehaviour
{
    public static float TileSize = 4f;
    public static int historyCount = 15;
    public List<UnitRuntime> occupants;
    public UnitRuntime plantOccupant;
    public MeshRenderer tileRenderer;
    public Vector2 location;
    public bool isMovable;
    public enum EdgeType { None, Top, Bottom, Left, Right, Corner }
    public GameObject tileNotVisibleObject;
    public ParticleSystem numberDisplay;
    public ParticleSystem energyParticles;
    public ParticleSystem poofParticles;
    public Transform numberParent;

    private LinkedList<Vector2> passerby;
    private UnitRuntime ambusher;

    public EdgeType Edge
    {
        get
        {
            EdgeType edge = EdgeType.None;

            bool isTop = location.y == GameManager.Instance.Map.yDimension - 1;
            bool isBot = location.y == 0;
            bool isLeft = location.x == GameManager.Instance.Map.xDimension - 1;
            bool isRight = location.x == 0;
            int edgeCount = 0;

            if (isTop)
            {
                edgeCount++;
                edge = EdgeType.Top;
            }
            if (isBot)
            {
                edgeCount++;
                edge = EdgeType.Bottom;
            }
            if (isLeft)
            {
                edgeCount++;
                edge = EdgeType.Left;
            }
            if (isRight)
            {
                edgeCount++;
                edge = EdgeType.Right;
            }
            if (edgeCount > 1)
            {
                edge = EdgeType.Corner;
            }
            return edge;
        }
    }

    public bool IsEdge
    {
        get
        {
            return !(Edge == EdgeType.None);
        }
    }

    public void Initialize(TerrainGrid map, int xCoordinate, int yCoordinate)
    {
        transform.SetParent(map.transform);
        location = new Vector2(xCoordinate, yCoordinate);
        float xPos = xCoordinate * TileSize;
        float yPos = yCoordinate * TileSize;
        transform.position = new Vector3(xPos, 0, yPos);
        passerby = new LinkedList<Vector2>();
        occupants = new List<UnitRuntime>();
    }

    public void RefreshTile()
    {
        ambusher = null;
        energyParticles.gameObject.SetActive(false);
        numberDisplay.gameObject.SetActive(false);
        poofParticles.gameObject.SetActive(false);

    }

    public void SetVisible(bool isVisible)
    {
        if (occupants.Count > 0)
        {
            foreach(UnitRuntime occupant in occupants)
            {
                occupant.gameObject.SetActive(isVisible);
            }
        }

        if (isVisible) tileNotVisibleObject.SetActive(false);
        else tileNotVisibleObject.SetActive(true);
    }

    public void ShowNumber(int number, Color color, float delay = 0f)
    {
        //numberDisplay
        number = Mathf.Clamp(number, 1, numberParent.childCount+1);
        for(int i = 0; i < numberParent.childCount; i++)
        {
            numberParent.GetChild(i).gameObject.SetActive(false);
        }
        numberParent.GetChild(number - 1).gameObject.SetActive(true);
        var main = numberDisplay.main;
        main.startDelay = delay;
        numberDisplay.gameObject.SetActive(true);
        numberDisplay.Play();
    }
    public int GetEnergyForPlayer(PlayerData player)
    {

        int localProd = 0;
        foreach (UnitRuntime o in occupants)
        {
            //energyProd += Mathf.Max(u.currentEnergy - u.data.maxEnergy, 0);
            if (o.owner == player)
            {

                localProd += o.CollectExcessEnergy();
            }
        }

        if(plantOccupant && plantOccupant.owner == player) localProd += plantOccupant.CollectExcessEnergy();


        if (localProd > 0)
        {
            energyParticles.gameObject.SetActive(true);

            energyParticles.Play();
            ShowNumber(localProd, Color.green, energyParticles.main.duration * .8f);
        }
        return localProd;
    }

    public void SetAmbush(UnitRuntime ambusher)
    {
        this.ambusher = ambusher;
    }

    public bool CanOccupy(UnitRuntime unit)
    {
        bool successful = false;

        if(!unit.data.isPlant)
        {
            successful = true;
        }
        else if (unit.data.isPlant && plantOccupant == null)
        {
            successful = true;
        }

        return successful;
    }

    public void Occupy(UnitRuntime unit)
    {
        if (!unit.data.isPlant)
        {
            occupants.Add(unit);
        }
        else if (unit.data.isPlant && plantOccupant == null)
        {
            plantOccupant = unit;
            tileRenderer.material = unit.data.worldMaterial;
        }
        poofParticles.gameObject.SetActive(true);

    }

    public void PassThrough(Vector2 direction, UnitRuntime unit)
    {
        if(unit.currentSize == UnitData.Size.Flying || unit.currentSize == UnitData.Size.Burrowed)
        {
            //doesn't leave tracks
            return;
        }

        //is there an ambush?
        if(ambusher)
        {
            AmbushAbility ambush = null;

            //woof this is hacky
            foreach(AbilityData a in ambusher.data.abilities)
            {
                if(a is AmbushAbility)
                {
                    ambush = (AmbushAbility)a;
                    break;
                }
            }

            if(ambush)
            {
                ambush.SpringAmbush(ambusher, unit, this);
            }
        }

        passerby.AddLast(direction * ((int)unit.currentSize + 1));
        if(passerby.Count > historyCount)
        {
            passerby.RemoveFirst();
        }
    }

    public Vector2[] GetHistory(int maxAge)
    {
        int count = Mathf.Min(maxAge, historyCount);
        Vector2[] tracks = new Vector2[count];

        for(int i = 0; i < count; i++)
        {
            tracks[i] = passerby.Last.Value;
            passerby.RemoveLast();
        }
        return tracks;
    }

    public void Vacate(UnitRuntime unit)
    {
        if(unit.data.isPlant)
        {
            plantOccupant = null;
        }
        else
        {
            occupants.Remove(unit);
        }
    }
}
