﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "NewProduceData", menuName = "Repair/Ability/ProduceData")]
public class ProduceAbility : AbilityData
{
    private bool produceComplete;
    private float currentProduceTime;

    public UnitData producedUnit;
    public int productionRange;
    public int minRange = 0;
    public bool replacesThisUnit;

    private bool hasProduced;

    public override void EnterActivate(UnitRuntime activator)
    {
        base.EnterActivate(activator);
        hasProduced = false;
        if (!replacesThisUnit && productionRange >= 1)
        {
            GameManager.Instance.DisplayOutline(activator.location, productionRange);
        }
        else
        {
            GameManager.Instance.DisplayOutline(activator.location, GridOutline.OutlineType.Green);
        }
    }

    public override void OnActivate(UnitRuntime activator)
    {
        if(replacesThisUnit)
        {
            GameManager.Instance.KillUnit(activator);
            GameManager.Instance.SpawnUnitForCurrentPlayer(producedUnit, activator.location);
            hasProduced = true;
        }
        else
        {
            TileRuntime tile = GameManager.Instance.UI.GetTileUnderMouse();
            if (tile == null)
            {
                return;
            }

            int dist = Mathf.FloorToInt(Vector2.Distance(activator.location, tile.location));
            if (dist <= productionRange && dist >= minRange)
            {
                GameManager.Instance.SpawnUnitForCurrentPlayer(producedUnit, tile.location);
                hasProduced = true;
            }
        }
    }

    public override void ExitActivate(UnitRuntime activator)
    {
        base.ExitActivate(activator);
        hasProduced = false;
    }

    public override bool IsFinished(UnitRuntime activator)
    {
        return hasProduced;
    }

    public void SpawnUnit(UnitData toSpawn)
    {

    }
}
